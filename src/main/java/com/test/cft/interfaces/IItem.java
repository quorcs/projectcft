package com.test.cft.interfaces;

import com.test.cft.bean.Item;

import java.util.Queue;

/**
 * Интерфейс для работы с очередью
 * Реализует метод добавления элемента в очередь
 */
public interface IItem {

	/**
	 * Добавление элемента в очередь
	 * @param item элемент
	 */
	void addItem(Item item);
}
