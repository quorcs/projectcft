package com.test.cft.interfaces;

import com.test.cft.bean.Item;
import com.test.cft.logic.ItemManager;

import java.util.Queue;

/**
 * Класс для реализации интерфейса работы с очередью
 * Переопределяет метод addItem для добавления элемента в очередь
 */
public class IItemImpl implements IItem {

	private ItemManager itemManager;

	public IItemImpl(ItemManager itemManager) {
		this.itemManager = itemManager;
	}

	@Override
	public void addItem(Item item) {
		itemManager.getQueue().add(item);
	}
}
