package com.test.cft.bean;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Класс-наследник Thread для обработки элементов очереди
 * Реализует метод добавления текущего элемента
 * Реализует метод обработки текущего элемента
 */
public class MyThread extends Thread {

	// группы, элементы которых обрабатываются в потоке
	// key - groupId элемента
	// value - itemId элемента
	private Map<Long, Long> groups = new ConcurrentHashMap<>();

	// элемент, который обрабатывается в текущий момент
	private Item currentItem;

	private Lock lock = new ReentrantLock();

	@Override
	public void run() {
		lock.lock();
		try {
			processElement();
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Получение групп, которые обрабатываются потоком
	 */
	public Map<Long, Long> getGroups() {
		return groups;
	}

	/**
	 * Установка текущего элемента
	 * @param currentItem элемент очереди
	 */
	public void setCurrentItem(Item currentItem) {
		this.currentItem = currentItem;
	}

	/**
	 * Обработка текущего элемента
	 */
	private void processElement() {
		if (currentItem != null) {
			// проверка на возможность обработки элемента
			if (!groups.containsKey(currentItem.getGroupId()) ||
					groups.get(currentItem.getGroupId()) < currentItem.getItemId()) {
				System.out.println(
						"Thread ID: " + getId() +
								" Thread Name: " + getName() +
								" Item(id: " + currentItem.getItemId() +
								", groupId: " + currentItem.getGroupId() + ")"
				);
				updateGroups();
			}
		}
	}

	/**
	 * Обновление последнего обработанного элемента группы
	 */
	private void updateGroups() {
		if (groups.containsKey(currentItem.getGroupId())) {
			groups.remove(currentItem.getGroupId());
		}
		groups.put(currentItem.getGroupId(), currentItem.getItemId());

		currentItem = null;
	}
}
