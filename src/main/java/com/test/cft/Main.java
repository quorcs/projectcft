package com.test.cft;

import com.test.cft.logic.ItemManager;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Класс, запускающий приложение
 * Реализует обработку входных параметров
 */
class Main {

	private static final Logger LOGGER = LogManager.getLogger(Main.class);

	public static void main(String[] args) throws ParseException, IOException, InterruptedException {

		LOGGER.trace("Start application ProjectCFT");
		LOGGER.trace("Enter parameters {}", (Object) args);

		// опции для запуска приложения
		Options options = new Options()
				.addOption("threads", true, "Max threads count")
				.addOption("h", false, "Help");

		CommandLine commandLine = new DefaultParser().parse(options, args);

		// обработка опции -h
		if (commandLine.hasOption("h") || args.length == 0) {
			printHelp(options);
		}

		// обработка опции -threads
		if (commandLine.hasOption("threads")) {
			new ItemManager(Long.parseLong(commandLine.getOptionValue("threads")));
		}
	}

	/**
	 * Вывод помощи по опциям в консоль
	 * @param options опции для запуска приложения
	 */
	private static void printHelp(Options options) {
		LOGGER.trace("Print help");
		String header = "\nEnter some parameters\n\n";
		String footer = "\nPlease report issues at valtario349@gmail.com";

		HelpFormatter helpFormatter = new HelpFormatter();
		helpFormatter.printHelp("ProjectCFT", header, options, footer, true);
	}
}