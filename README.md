The test project for CFT company.

At startup the application you need add the input parameter:  
**`-h`** - for oversee the help  
**`-threads <arg>`** - for entering max threads count
                    
The **`appWithAutogenerate`** branch implements application with using autogeneration of elements.
                    
The **`master`** branch implements classic application without autogeneration of elements.
                    
If you want use the **`master`** branch you must to use IItem interface for adding elements to queue. 
Otherwise thread will works but it will be useless.
                    
If you uses the **`appWithAutogenerate`** branch you must just start the application 
and monitor it work.