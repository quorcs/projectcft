package com.test.cft.logic;

import com.test.cft.bean.Item;
import com.test.cft.bean.MyThread;
import com.test.cft.interfaces.IItem;
import com.test.cft.interfaces.IItemImpl;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Класс-менеджер для работы с элементами
 * Реализует обработку элементов очереди
 */
@Getter
@Setter
@NoArgsConstructor
public class ItemManager {

	private static final Logger LOGGER = LogManager.getLogger(ItemManager.class);

	// список потоков
	private List<MyThread> threads = new ArrayList<>();

	// интерфейс
	private IItem iItem;

	// основная очередь
	private Queue<Item> queue = new ConcurrentLinkedQueue<>();

	// максимальное количество потоков
	private long maxThreadsCount;

	public ItemManager(long maxThreadsCount) {
		Init(maxThreadsCount);
	}

	/**
	 * Инициализация класса
	 * @param maxThreadsCount максимальное количество потоков
	 */
	private void Init(long maxThreadsCount) {
		// запускаем поток для слушателя командной строки
		new CommandLineListener().start();

		this.maxThreadsCount = maxThreadsCount;
		iItem = new IItemImpl(this);

		process();
	}

	/**
	 * Метод для обработки элементов очереди
	 */
	private synchronized void process() {
		while (true) {
			// пока очередь не пустая, обрабатываем элементы
			while (!queue.isEmpty()) {
				MyThread thread = getThread(queue.peek());

				if (thread == null) {
					if (threads.size() == maxThreadsCount) {
						thread = threads.get(new Random().nextInt(threads.size()));
					} else {
						thread = new MyThread();
						thread.setName("Thread #" + (threads.size() + 1));
						thread.start();
						threads.add(thread);
					}
				}

				thread.setCurrentItem(queue.remove());
				thread.run();
			}

			// пока очередь пустая, ждем добавления нового элемента в очередь
			while (queue.isEmpty()) {
				Thread.yield();
			}
		}
	}

	/**
	 * Выбор потока для обработки элемента
	 * @param item элемент
	 */
	private MyThread getThread(Item item) {
		MyThread thread = null;

		for (MyThread myThread : threads) {
			if (myThread.getGroups().keySet().contains(item.getGroupId())) {
				thread = myThread;
				break;
			}
		}

		return thread;
	}
}
