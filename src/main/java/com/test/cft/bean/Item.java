package com.test.cft.bean;

import lombok.*;

/**
 * Класс-объект элемента очереди
 * @attribute itemId идентификатор элемента
 * @attribute groupId идентификатор группы, к которой принадлежит элемент
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Item {

	private long itemId;
	private long groupId;
}
