package com.test.cft.logic;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

/**
 * Класс-слушатель для командной строки
 * Обрабатывает ввод текста с клавиатуры
 * Реализует возможность прекратить работу приложения
 */
public class CommandLineListener extends Thread {

	private static final Logger LOGGER = LogManager.getLogger(CommandLineListener.class);

	CommandLineListener() {
		setDaemon(true);
	}

	@Override
	public void run() {
		while (true) {
			Scanner scanner = new Scanner(System.in);

			if (scanner.next().toLowerCase().equals("exit")) {
				LOGGER.trace("Cancelling the application");
				System.exit(1);
			}
		}
	}
}
